from src.models.apple_payment_gateway import ApplePaymentGateway
from src.models.apple_payment_request import ApplePaymentRequest
from datetime import datetime


def getAppleReceipt_legacycodefunction(request: ApplePaymentRequest):
    sendEventToAmplitude('get_eceipt_event')
    sendStartTimeLogToSentry(datetime.now())

    gateway = ApplePaymentGateway()
    gateway.setRequest(request)
    # Verify and fetch receipt from Apple Payment Server
    receipt = gateway.getReceipt()

    sendEndTimeLogToSentry(datetime.now())

    return receipt


def sendEventToAmplitude(event: str):
    # sending an event onto Amplitude server
    pass
def sendStartTimeLogToSentry(dt: datetime):
    # sending a log onto Sentry server
    pass
def sendEndTimeLogToSentry(dt: datetime):
    # sending a log onto Sentry server
    pass
