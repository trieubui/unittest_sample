class AppleReceiptException(ValueError):

    def __init__(self, errcode: int):
        self._errcode = errcode

    @property
    def errMessage(self):
        messages = {
            0: "Valid",
            100: "Expired",
            21000: "The request to the App Store did not use the HTTP POST request method.",
            21001: "The App Store no longer sends this status code.",
            21002: "Invalid Apple receipt",
            21003: "The system could not authenticate the receipt.",
            21004: "The shared secret you provided does not match the shared secret on file for your account.",
            21005: "The receipt server was temporarily unable to provide the receipt. Try again.",
            21006: "This receipt is valid, but the subscription is in an expired state. When your server receives this status code, the system also decodes and returns receipt data as part of the response. This status only returns for iOS 6-style transaction receipts for auto-renewable subscriptions.",
            21007: "This receipt is from the test environment, but you sent it to the production environment for verification.",
            21008: "This receipt is from the production environment, but you sent it to the test environment for verification.",
            21009: "Internal data access error. Try again later.",
            21010: "The system cannot find the user account or the user account has been deleted.",
        }
        if self._errcode not in messages:
            return "Unknown error. Please contact to administrator."
        return messages[self._errcode]
