from .apple_receipt import AppleReceipt
from .apple_payment_request import ApplePaymentRequest
from src.exceptions import AppleReceiptException


class ApplePaymentGateway:
    _request: ApplePaymentRequest

    def setRequest(self, request: ApplePaymentRequest):
        self._request = request

    def getReceipt(self) -> AppleReceipt:
        responseJSON = self._request.verifyReceipt().json()
        receiptStatus = responseJSON.get('status')

        if receiptStatus != 0:
            raise AppleReceiptException(receiptStatus)

        receiptDict = responseJSON.get('latest_receipt_info')

        return AppleReceipt(**receiptDict[0])
