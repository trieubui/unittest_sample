import requests
from src.defines import APPLE_RECEIPT_PASSWORD, APPLE_PAYMENT_BASE_URL


class ApplePaymentRequest:

    _receiptData: str

    def __init__(self, receiptData: str):
        self._receiptData = receiptData

    def verifyReceipt(self) -> requests.Response:
        # Send a request to Apple Payment to get the verified receipt
        return requests.post(
            f'{APPLE_PAYMENT_BASE_URL}/verifyReceipt',
            json={
                "receipt-data": self._receiptData,
                "exclude-old-transactions": False,
                "password": APPLE_RECEIPT_PASSWORD
            }
        )
