from pydantic import BaseModel, Field
from typing import Optional


class AppleReceipt(BaseModel):
    product_id: str = Field()
    transaction_id: str = Field()
    original_transaction_id: str = Field()
    purchase_date: str = Field()
    purchase_date_ms: str = Field()
    purchase_date_pst: str = Field()
    original_purchase_date: str = Field()
    original_purchase_date_ms: str = Field()
    original_purchase_date_pst: str = Field()
    is_trial_period: Optional[str] = Field(default="false")
    is_in_intro_offer_period: Optional[str] = Field(default="false")
    is_upgraded: Optional[str] = Field(default="false")
    in_app_ownership_type: Optional[str] = Field()
