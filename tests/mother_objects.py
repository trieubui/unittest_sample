from requests import Request
from src.models.apple_payment_request import ApplePaymentRequest


class ApplePaymentRequestMother:
    @staticmethod
    def validLifeTimeRequest() -> Request:
        return ApplePaymentRequest('valid_lifetime')

    @staticmethod
    def invalidLifeTimeRequest() -> Request:
        return ApplePaymentRequest('invalid')
