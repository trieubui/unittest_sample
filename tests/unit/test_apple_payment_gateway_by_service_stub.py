from src.models.apple_payment_gateway import ApplePaymentGateway
from src.models.apple_receipt import AppleReceipt
from src.models.apple_payment_request import ApplePaymentRequest
from src.exceptions import AppleReceiptException


def test_valid_apple_request_by_service_stub():
    gateway = ApplePaymentGateway()
    # valid_lifetime is a receipt hash is already in wiremock docker
    req = ApplePaymentRequest('valid_lifetime')
    gateway.setRequest(req)
    assert isinstance(gateway.getReceipt(), AppleReceipt) is True


def test_invalid_apple_request_by_service_stub():
    gateway = ApplePaymentGateway()
    # invalid is a receipt hash is already in wiremock docker
    req = ApplePaymentRequest('invalid')
    gateway.setRequest(req)

    try:
        gateway.getReceipt()
        assert False, "Should has a exception here."
    except AppleReceiptException as e:
        assert e.errMessage == 'Invalid Apple receipt'
