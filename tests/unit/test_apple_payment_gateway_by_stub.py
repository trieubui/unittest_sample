from src.models.apple_payment_gateway import ApplePaymentGateway
from src.models.apple_receipt import AppleReceipt
from src.exceptions import AppleReceiptException
from requests import Response
from src.models.apple_payment_request import ApplePaymentRequest


def test_valid_apple_request_by_stub():
    class ValidApplePaymentRequestStub(ApplePaymentRequest):
        def __init__(self):
            super(ValidApplePaymentRequestStub, self).__init__('valid')

        def verifyReceipt(self) -> Response:
            res = Response()
            res.status_code = 200
            res._content = b'{"receipt":{"receipt_type":"Production","adam_id":1083804886,"app_item_id":1083804886,"bundle_id":"com.us.nobarriers.elsa-ios","application_version":"1987","download_id":88033541549625,"version_external_identifier":850621350,"receipt_creation_date":"2022-07-31 01:34:20 Etc/GMT","receipt_creation_date_ms":"1659231260000","receipt_creation_date_pst":"2022-07-30 18:34:20 America/Los_Angeles","request_date":"2022-08-18 08:01:13 Etc/GMT","request_date_ms":"1660809673138","request_date_pst":"2022-08-18 01:01:13 America/Los_Angeles","original_purchase_date":"2018-07-03 05:34:23 Etc/GMT","original_purchase_date_ms":"1530596063000","original_purchase_date_pst":"2018-07-02 22:34:23 America/Los_Angeles","original_application_version":"24","in_app":[{"quantity":"1","product_id":"lifetime.ios.v2_membership","transaction_id":"480000642668872","original_transaction_id":"480000642668872","purchase_date":"2020-06-27 00:21:07 Etc/GMT","purchase_date_ms":"1593217267000","purchase_date_pst":"2020-06-26 17:21:07 America/Los_Angeles","original_purchase_date":"2020-06-27 00:21:07 Etc/GMT","original_purchase_date_ms":"1593217267000","original_purchase_date_pst":"2020-06-26 17:21:07 America/Los_Angeles","is_trial_period":"false","in_app_ownership_type":"PURCHASED"}]},"environment":"Production","latest_receipt_info":[{"quantity":"1","product_id":"lifetime.ios.v2_membership","transaction_id":"480000642668872","original_transaction_id":"480000642668872","purchase_date":"2020-06-27 00:21:07 Etc/GMT","purchase_date_ms":"1593217267000","purchase_date_pst":"2020-06-26 17:21:07 America/Los_Angeles","original_purchase_date":"2020-06-27 00:21:07 Etc/GMT","original_purchase_date_ms":"1593217267000","original_purchase_date_pst":"2020-06-26 17:21:07 America/Los_Angeles","is_trial_period":"false","in_app_ownership_type":"PURCHASED"}],"latest_receipt":"valid","status":0}'
            return res

    gateway = ApplePaymentGateway()
    # ValidApplePaymentRequestStub extends from ApplePaymentRequest
    stub = ValidApplePaymentRequestStub()
    gateway.setRequest(stub)
    assert isinstance(gateway.getReceipt(), AppleReceipt) is True


def test_invalid_apple_request_by_stub():
    class InvalidApplePaymentRequestStub(ApplePaymentRequest):
        def __init__(self):
            super(InvalidApplePaymentRequestStub, self).__init__('valid')

        def verifyReceipt(self) -> Response:
            res = Response()
            res.status_code = 200
            res._content = b'{"status":21002}'
            return res
    gateway = ApplePaymentGateway()

    # InvalidApplePaymentRequestStub extends from ApplePaymentRequest
    stub = InvalidApplePaymentRequestStub()
    gateway.setRequest(stub)

    try:
        gateway.getReceipt()
        assert False, "Should has a exception here."
    except AppleReceiptException as e:
        assert e.errMessage == 'Invalid Apple receipt'
